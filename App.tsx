import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import LoginScreen from './src/screens/Login/Login.screen';
import HomeScreen from './src/screens/Home/Home.screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {PaperProvider} from 'react-native-paper';
import ListCatatan from './src/screens/ListCatatan/ListCatatan';
import {store} from './src/redux/store.js';
import {Provider} from 'react-redux';
import FormCatatan from './src/screens/FormCatatan/FormCatatan';

const Stack = createNativeStackNavigator();
function App(): React.JSX.Element {
  return (
    <Provider store={store}>
      <PaperProvider>
        <SafeAreaView style={{flex: 1}}>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName="Login"
              screenOptions={{
                headerShown: false,
                animation: 'fade_from_bottom',
              }}>
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="ListCatatan" component={ListCatatan} />
              <Stack.Screen name="FormCatatan" component={FormCatatan} />
              <Stack.Screen name="Login" component={LoginScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaView>
      </PaperProvider>
    </Provider>
  );
}

export default App;
