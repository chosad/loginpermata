import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {Checkbox} from 'react-native-paper';

const ListCheckBox = () => {
  const [checked, setChecked] = useState(false);
  return (
    <View>
      <View style={styles.flexAlignCenter}>
        <Checkbox
          status={checked ? 'checked' : 'unchecked'}
          onPress={() => {
            setChecked(!checked);
          }}
        />
        <Text>CheckBox Normal</Text>
      </View>
      <View style={styles.flexAlignCenter}>
        <Checkbox status={'checked'} disabled />
        <Text>CheckBox Disabled True</Text>
      </View>
      <View style={styles.flexAlignCenter}>
        <Checkbox status={'unchecked'} disabled />
        <Text>CheckBox Disabled False</Text>
      </View>
    </View>
  );
};

export default ListCheckBox;

const styles = StyleSheet.create({
  flexAlignCenter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
