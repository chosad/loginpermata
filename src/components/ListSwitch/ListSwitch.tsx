import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Switch} from 'react-native-paper';

const ListSwitch = () => {
  const [isSwitchOn, setIsSwitchOn] = React.useState(false);
  return (
    <View>
      <View style={styles.flexAlignCenter}>
        <Switch
          value={isSwitchOn}
          onValueChange={() => setIsSwitchOn(!isSwitchOn)}
        />
        <Text>Switch On Off</Text>
      </View>
      <View style={styles.flexAlignCenter}>
        <Switch value={isSwitchOn} disabled />
        <Text>Switch On Off Disabled</Text>
      </View>
    </View>
  );
};

export default ListSwitch;

const styles = StyleSheet.create({
  flexAlignCenter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
