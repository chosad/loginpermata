import {StyleSheet, View} from 'react-native';
import React from 'react';
import {Button} from 'react-native-paper';

const ListButton = () => {
  return (
    <View style={styles.container}>
      <Button mode="text" onPress={() => console.log('Pressed')}>
        Button text
      </Button>
      <Button mode="contained" onPress={() => console.log('Pressed')}>
        Button Contained
      </Button>
      <Button mode="outlined" onPress={() => console.log('Pressed')}>
        Button Outlined
      </Button>
      <Button mode="elevated" disabled onPress={() => console.log('Pressed')}>
        Button elevated disabled
      </Button>
    </View>
  );
};

export default ListButton;

const styles = StyleSheet.create({
  container: {
    gap: 10,
  },
});
