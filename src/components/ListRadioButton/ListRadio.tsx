import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {RadioButton} from 'react-native-paper';

const ListRadio = () => {
  const [radio, setRadio] = React.useState('first');
  return (
    <View>
      <View style={styles.flexAlignCenter}>
        <RadioButton
          value="first"
          status={radio === 'first' ? 'checked' : 'unchecked'}
          onPress={() => setRadio('first')}
        />
        <Text>Radio First</Text>
      </View>
      <View style={styles.flexAlignCenter}>
        <RadioButton
          value="second"
          status={radio === 'second' ? 'checked' : 'unchecked'}
          onPress={() => setRadio('second')}
        />
        <Text>Radio Second</Text>
      </View>
      <View style={styles.flexAlignCenter}>
        <RadioButton value="third" status={'checked'} disabled />
        <Text>Radio Third Disabled</Text>
      </View>
    </View>
  );
};

export default ListRadio;

const styles = StyleSheet.create({
  flexAlignCenter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
