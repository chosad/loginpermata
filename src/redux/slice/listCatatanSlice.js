import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  list: [],
};

export const listCatatanSlice = createSlice({
  name: 'listCatatan',
  initialState,
  reducers: {
    addList: (state, action) => {
      state.list.push(action.payload);
    },
    removeList: (state, action) => {
      const indexRemove = state.list.findIndex(
        item => item.id === action.payload,
      );
      state.list.splice(indexRemove, 1);
    },
    editList: (state, action) => {
      const indexEdit = state.list.findIndex(
        item => item.id === action.payload.id,
      );
      state.list.splice(indexEdit, 1, action.payload);
    },
  },
});

export const {addList, removeList, editList} = listCatatanSlice.actions;

export default listCatatanSlice.reducer;
