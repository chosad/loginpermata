import {configureStore} from '@reduxjs/toolkit';
import listCatatanSlice from './slice/listCatatanSlice';

export const store = configureStore({
  reducer: {
    listCatatan: listCatatanSlice,
  },
});
