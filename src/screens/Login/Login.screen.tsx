import React, {useRef, useState} from 'react';
import {Alert, Image, StyleSheet, View} from 'react-native';
import GoogleRecaptcha from 'react-native-google-recaptcha';
import {Button, TextInput} from 'react-native-paper';
import PermataBankLogo from '../../assets/logo-permata-bank.png';

const LoginScreen = ({navigation}: any) => {
  const [username, setUsername] = useState('permata');
  const [password, setPassword] = useState('mobile');
  const [hide, setHide] = useState(true);

  const ValidateLogin = () => {
    if (username === 'permata' && password === 'mobile') {
      send();
    } else {
      Alert.alert('Username atau Password Salah!');
    }
  };

  const recaptcha = useRef();

  const send = () => {
    recaptcha.current.open();
  };

  const onVerify = () => {
    navigation.navigate('ListCatatan');
  };

  const onExpire = () => {
    console.warn('expired!');
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={PermataBankLogo}
          alt="logo-permata-bank"
          style={styles.image}
        />
      </View>
      <TextInput
        label="Nama Pengguna"
        mode="outlined"
        value={username}
        onChangeText={value => setUsername(value)}
      />
      <TextInput
        label="Kata Sandi"
        mode="outlined"
        secureTextEntry={hide}
        value={password}
        onChangeText={value => setPassword(value)}
        right={
          <TextInput.Icon
            icon={hide ? 'eye' : 'eye-off'}
            onPress={() => setHide(!hide)}
          />
        }
      />

      <Button mode="elevated" onPress={ValidateLogin} style={styles.btn}>
        Masuk
      </Button>

      <GoogleRecaptcha
        ref={recaptcha}
        baseUrl="http://localhost:3000"
        onError={onExpire}
        onVerify={onVerify}
        siteKey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
      />
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    display: 'flex',
    gap: 10,
  },
  image: {
    width: 200,
    objectFit: 'contain',
  },
  containerImage: {
    display: 'flex',
    alignItems: 'center',
  },
  btn: {
    marginTop: 10,
  },
});
