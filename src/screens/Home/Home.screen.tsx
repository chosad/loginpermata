import React, {useState} from 'react';
import {Dimensions, ScrollView, StyleSheet, Text, View} from 'react-native';
import ListButton from '../../components/ListButton/ListButton';
import ListCheckBox from '../../components/ListCheckBox/ListCheckBox';
import ListRadio from '../../components/ListRadioButton/ListRadio';
import ListSwitch from '../../components/ListSwitch/ListSwitch';
import {IconButton, Modal, Portal, TouchableRipple} from 'react-native-paper';

const HomeScreen = () => {
  const [visible, setVisible] = useState(false);
  const [component, setComponent] = useState('');
  const screenWidth = Dimensions.get('window').width;
  const boxWidth = (screenWidth - 60) / 2;
  const componentBase = {
    Button: <ListButton />,
    CheckBox: <ListCheckBox />,
    Radio: <ListRadio />,
    Switch: <ListSwitch />,
  };
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textHeader}>List Component</Text>
      <View style={styles.containerTouchable}>
        {['Button', 'CheckBox', 'Radio', 'Switch'].map(item => (
          <TouchableRipple
            onPress={() => {
              setVisible(true);
              setComponent(item);
            }}
            rippleColor="rgba(0, 0, 0, .32)"
            style={[styles.box, {width: boxWidth}]}>
            <Text>{item}</Text>
          </TouchableRipple>
        ))}
      </View>
      <Portal>
        <Modal
          visible={visible}
          onDismiss={() => setVisible(false)}
          contentContainerStyle={styles.containerModal}
          style={{margin: 20}}>
          <View>
            <View style={{}}>
              <IconButton
                icon="close"
                style={styles.iconClose}
                size={30}
                onPress={() => setVisible(false)}
              />
              <Text style={styles.headerModal}>{component} Component</Text>
            </View>
          </View>
          {componentBase[component]}
        </Modal>
      </Portal>
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    gap: 20,
  },
  textHeader: {
    fontSize: 20,
    textAlign: 'center',
    margin: 20,
  },
  containerTouchable: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  containerModal: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  },
  iconClose: {
    alignSelf: 'flex-end',
    width: 50,
    marginTop: -15,
    marginRight: -15,
  },
  headerModal: {
    alignSelf: 'center',
    fontSize: 18,
    marginTop: -40,
    marginBottom: 20,
  },
  box: {
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 4,
  },
  flexAlignCenter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
