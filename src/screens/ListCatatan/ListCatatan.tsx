/* eslint-disable react-native/no-inline-styles */
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Icon} from 'react-native-paper';
import {useSelector} from 'react-redux';

const ListCatatan = ({navigation}: any) => {
  const listCatatan = useSelector((state: any) => state.listCatatan.list);
  const [copyListCatatan, setCopyListCatatan] = useState([]);
  const [totalPemasukan, setTotalPemasukan] = useState('');
  const [totalPengeluaran, setTotalPengeluaran] = useState('');

  useEffect(() => {
    let totalPemasukan1 = 0;
    let totalPengeluaran1 = 0;
    const hitungTotal = async () => {
      const pemasukanArr = await listCatatan.filter(
        (item: any) => item.type === 'Pemasukan',
      );
      const pengeluaranArr = await listCatatan.filter(
        (item: any) => item.type === 'Pengeluaran',
      );
      await pemasukanArr?.forEach(
        (item: any) => (totalPemasukan1 += Number(item.jumlah)),
      );
      await pengeluaranArr?.forEach(
        (item: any) => (totalPengeluaran1 += Number(item.jumlah)),
      );
      setTotalPemasukan(totalPemasukan1.toString());
      setTotalPengeluaran(totalPengeluaran1.toString());
    };
    hitungTotal();
  }, [listCatatan]);

  useEffect(() => {
    setCopyListCatatan(listCatatan.slice());
  }, [listCatatan]);

  const renderItem = ({item}: any) => {
    const tahun = item.date.getFullYear();
    const bulan = item.date.getMonth() + 1;
    const tanggal = item.date.getDate();
    return (
      <TouchableOpacity
        style={styles.cardListCashflow}
        onPress={() =>
          navigation.navigate('FormCatatan', {
            type: item.type,
            mode: 'Edit',
            data: item,
          })
        }>
        <View style={styles.cardInfo}>
          <View style={styles.areaTanggal}>
            <Text style={styles.textTanggal}>
              {tanggal.toString().length === 1 ? `0${tanggal}` : tanggal}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text>{bulan.toString().length === 1 ? `0${bulan}` : bulan}</Text>
              <Text>-{tahun}</Text>
            </View>
          </View>
          <View style={styles.areaNominal}>
            <Icon
              source={item.type === 'Pemasukan' ? 'arrow-down' : 'arrow-up'}
              size={25}
              color={item.type === 'Pemasukan' ? 'green' : 'red'}
            />
            <View>
              <Text style={{fontSize: 18, color: 'black'}}>
                {item.kategori}
              </Text>
              <Text style={{fontSize: 12}}>
                {item.keterangan === '' ? '-' : item.keterangan}
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text style={{color: item.type === 'Pemasukan' ? 'green' : 'red'}}>
            Rp. {item.jumlah.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Catatan Keuangan</Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
        }}>
        <Button
          title="Tambah Pemasukan"
          color="green"
          onPress={() =>
            navigation.navigate('FormCatatan', {
              type: 'Pemasukan',
              mode: 'Buat',
            })
          }
        />
        <Button
          title="Tambah Pengeluaran"
          color="red"
          onPress={() =>
            navigation.navigate('FormCatatan', {
              type: 'Pengeluaran',
              mode: 'Buat',
            })
          }
        />
      </View>
      <View style={styles.areaTotal}>
        <View style={styles.cardTotal}>
          <Text style={{fontSize: 12}}>Total Pemasukan</Text>
          <Text style={{fontSize: 18, color: 'green'}}>
            Rp. {totalPemasukan.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
          </Text>
        </View>
        <View style={[styles.cardTotal, {alignItems: 'flex-end'}]}>
          <Text style={{fontSize: 12}}>Total Pengeluaran</Text>
          <Text style={{fontSize: 18, color: 'red'}}>
            Rp. {totalPengeluaran.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
          </Text>
        </View>
      </View>
      <FlatList
        data={copyListCatatan.sort(
          (a: any, b: any) =>
            new Date(b.date).getDate() - new Date(a.date).getDate(),
        )}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        contentContainerStyle={{paddingBottom: 200}}
      />
    </View>
  );
};

export default ListCatatan;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  areaNominal: {flexDirection: 'row', alignItems: 'center', gap: 15},
  textTanggal: {fontSize: 20, color: 'black'},
  areaTanggal: {justifyContent: 'center', alignItems: 'center'},
  areaTotal: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 10,
  },
  cardInfo: {flexDirection: 'row', gap: 30, alignItems: 'center'},
  textHeader: {
    fontSize: 25,
    textAlign: 'center',
  },
  cardListCashflow: {
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    margin: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cardTotal: {
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    width: '47%',
  },
});
