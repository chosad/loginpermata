/* eslint-disable react-native/no-inline-styles */
import {
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Modal,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import DatePicker from 'react-native-date-picker';
import {useDispatch} from 'react-redux';
import {
  addList,
  editList,
  removeList,
} from '../../redux/slice/listCatatanSlice';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {IconButton} from 'react-native-paper';

const FormCatatan = ({navigation, route}: any) => {
  const {type, mode, data} = route.params;
  const dispatch = useDispatch();
  const [tanggal, setTanggal] = useState('');
  const [date, setDate] = useState(new Date());
  const [modalDatePicker, setModalDatePicker] = useState(false);
  const [modalKategori, setModalKategori] = useState(false);

  const [kategori, setKategori] = useState('');
  const [jumlah, setJumlah] = useState('');
  const [onBlur, setOnBlur] = useState(true);

  const [keterangan, setKeterangan] = useState('');

  const init = () => {
    setJumlah(data.jumlah);
    setDate(data.date);
    setKategori(data.kategori);
    setKeterangan(data.keterangan);
    setTanggal(data.tanggal);
    formatJumlah();
  };

  const formatJumlah = () => {
    if (onBlur) {
      setJumlah(jumlahTerbaru =>
        jumlahTerbaru.replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
      );
    } else {
      setJumlah(jumlahTerbaru => jumlahTerbaru.replace('.', ''));
    }
  };

  useEffect(() => {
    if (data) {
      init();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    formatJumlah();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onBlur, jumlah]);

  const handleSimpan = () => {
    if (mode === 'Edit') {
      // if (
      //   tanggal === data.tanggal ||
      //   kategori === data.kategori ||
      //   jumlah === data.kategori ||
      //   keterangan === data.keterangan
      // ) {
      //   console.log('');
      // } else {
      dispatch(
        editList({
          id: data.id,
          tanggal,
          date,
          kategori,
          keterangan,
          jumlah: jumlah.replaceAll('.', ''),
          type,
        }),
      );
      // }
    } else {
      dispatch(
        addList({
          id: uuidv4(),
          tanggal,
          date,
          kategori,
          keterangan,
          jumlah: jumlah.replaceAll('.', ''),
          type,
        }),
      );
    }
    navigation.goBack();
  };

  const handleHapus = () => {
    dispatch(removeList(data.id));
    navigation.goBack();
  };

  const baseCategory = {
    Pemasukan: ['Umum', 'Gaji', 'Bonus', 'Hadiah'],
    Pengeluaran: ['Umum', 'Rumah', 'Makanan', 'Sedekah'],
  };

  return (
    <View>
      <View
        style={[
          styles.header,
          type === 'Pemasukan'
            ? {backgroundColor: 'green'}
            : {backgroundColor: 'red'},
        ]}>
        <Text style={styles.headerText}>
          {mode} {type}
        </Text>
      </View>
      <ScrollView style={styles.container}>
        <View style={styles.cardForm}>
          <View style={{marginBottom: 15}}>
            <Text style={{marginBottom: 3}}>Tanggal*</Text>
            <TextInput
              style={styles.inputStyle}
              onPressIn={() => setModalDatePicker(true)}
              value={tanggal}
              inputMode="none"
            />
          </View>
          <View style={{marginBottom: 15}}>
            <Text style={{marginBottom: 3}}>Kategori {type}*</Text>
            <TextInput
              style={styles.inputStyle}
              onPressIn={() => setModalKategori(true)}
              value={kategori}
              inputMode="text"
            />
          </View>
          <View style={{marginBottom: 15}}>
            <Text style={{marginBottom: 3}}>Jumlah*</Text>
            <TextInput
              style={styles.inputStyle}
              onBlur={() => setOnBlur(true)}
              onFocus={() => setOnBlur(false)}
              onChangeText={setJumlah}
              value={jumlah}
              inputMode="numeric"
            />
          </View>
          <View style={{marginBottom: 15}}>
            <Text style={{marginBottom: 3}}>Keterangan</Text>
            <TextInput
              style={{
                backgroundColor: '#eeeeee',
                borderRadius: 10,
                padding: 10,
              }}
              onChangeText={setKeterangan}
              value={keterangan}
              inputMode="text"
              multiline
              numberOfLines={5}
            />
          </View>
        </View>
      </ScrollView>
      <View style={styles.areaBtn}>
        <Button
          title={mode === 'Edit' ? 'Ubah' : 'Simpan'}
          onPress={() => handleSimpan()}
          disabled={tanggal === '' || jumlah === '' || kategori === ''}
        />
        {mode === 'Edit' && (
          <Button title={'Hapus'} onPress={() => handleHapus()} color="red" />
        )}
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalKategori}
        onRequestClose={() => {
          setModalKategori(false);
        }}>
        <View style={styles.centerModalKategori}>
          <View style={styles.cardModalKategori}>
            <IconButton
              icon="close"
              style={styles.closeModalKategori}
              size={30}
              onPress={() => setModalKategori(false)}
            />
            {baseCategory[type].map((item: any) => (
              <TouchableOpacity
                onPress={() => {
                  setKategori(item);
                  setModalKategori(false);
                }}
                style={styles.modalBtnKategori}>
                <Text>{item}</Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </Modal>
      <DatePicker
        modal
        open={modalDatePicker}
        date={date}
        onConfirm={date1 => {
          setModalDatePicker(false);
          setTanggal(
            `${date1.getDate()}-${date1.getMonth() + 1}-${date1.getFullYear()}`,
          );
          setDate(date1);
        }}
        onCancel={() => {
          setModalDatePicker(false);
        }}
      />
    </View>
  );
};

export default FormCatatan;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  header: {
    backgroundColor: '#8ec3b3',
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {fontSize: 18, color: 'white'},
  inputStyle: {
    backgroundColor: '#eeeeee',
    borderRadius: 10,
    padding: 10,
  },
  areaBtn: {paddingHorizontal: 30, gap: 10},
  cardForm: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
  },
  centerModalKategori: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  cardModalKategori: {
    backgroundColor: 'white',
    width: '70%',
    borderRadius: 10,
    padding: 20,
    elevation: 7,
  },
  closeModalKategori: {
    alignSelf: 'flex-end',
    marginTop: -15,
    marginRight: -15,
  },
  modalBtnKategori: {
    padding: 10,
    backgroundColor: '#F7F7F7',
    margin: 5,
    borderRadius: 10,
  },
});
